const express = require("express");

const router = express.Router();
const reward = require("../repo/reward.repo");
const errorCode = require("../common/error");
const errorhandler = require("../common/errorhandler");
const successhandler = require("../common/successhandler");

router.post("/", async (req, res, next) => {
    try {
  
      const result = await reward.postReward(req.body);
  
      const resobj = {
        body: {
          reward: result,
        },
      };
  
      return successhandler.sendSuccessResponse(resobj, res);
  
    } catch (error) {
      next(error);
    }
    return true;
  });

router.get("/", async (req, res, next) => {
    try {
     const result = await reward.findRewards();
  
      const resobj = {
        body: {
          rewards: result,
        },
      };
  
      return successhandler.sendSuccessResponse(resobj, res);
  
    } catch (error) {
      next(error);
    }
    return true;
  });

router.get("/:rewardId/claps", async (req, res, next) => {
    try {
     const result = await reward.findRewardClaps(req.params.rewardId);
  
      const resobj = {
        body: {
          rewards: result,
        },
      };
  
      return successhandler.sendSuccessResponse(resobj, res);
  
    } catch (error) {
        console.log(error)
      next(error);
    }
    return true;
  });

router.patch("/:rewardId", async (req, res, next) => {
    try {
  
      const result = await reward.patchReward(req.body,req.params.rewardId);
  
      const resobj = {
        body: {
          reward: result,
        },
      };
  
      return successhandler.sendSuccessResponse(resobj, res);
  
    } catch (error) {
        console.log(error)
      next(error);
    }
    return true;
  });
  
  router.get("/:rewardId/comments", async (req, res, next) => {
    try {
     const result = await reward.findRewardComments(req.params.rewardId);
  
      const resobj = {
        body: {
          rewards: result,
        },
      };
  
      return successhandler.sendSuccessResponse(resobj, res);
  
    } catch (error) {
        console.log(error)
      next(error);
    }
    return true;
  });

module.exports = router;