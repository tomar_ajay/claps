const express = require("express");

const router = express.Router();

const rewardCommentController = require("../controllers/index").rewardComment;

router.use("/rewardComments", rewardCommentController);

module.exports = router;