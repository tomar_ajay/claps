/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const RewardsWrapper = styled.div`
.reward-wrapper {
    min-height: 100vh;
    position: relative;
    top: 0;
    display: flex;
    @media only screen and (min-width:992px){
      display: flex;
      top: 0;
    }
  }
  .reward-wrapper-left {
    background-size: cover;
    @media only screen and (min-width:992px){
      width: 60%;
      height: 100vh;
      display: block;
      justify-content: center;
      align-items: center;
      margin-bottom: 0px;
      text-align: center;
    }
    }
  }
  .reward-wrapper-left img {
    height:100%;
  }
  .reward-wrapper-right {
    width: 100%;
    height: 100vh;
    border-radius: 11px;
    opacity: 1;
    display: block;
    justify-content: center;
    align-items: center;
    @media only screen and (min-width:992px){
      min-height: 100vh;
      width: 50%;
      position: static;
      border-radius: 0;
      box-shadow: none;
    }
    @media only screen and (min-width:1200px){
      min-height: 100vh;
     
      position: static;
      border-radius: 0;
      box-shadow: none;
    }
  }
  .claps-logo {
    background: url(../images/logo.png) no-repeat;
    background-size: contain;
    width: 20rem;
    min-height: 5.5625rem;
    position: relative;
    color: #222328;
  }
  a {
    color: #000 !important;
    text-decoration: none;
}
  .home-content {
    background: linear-gradient(
        180deg
        , #dbdbdb 0%, #65156f 100%);
    padding: 20px;
    height: 100%;
  }
  .home-content h1 {
    font-family: 'Allison', cursive;
    font-size: 55px;
    color: #fff;
    text-align: center;
  }
  .MuiButton-contained {
    background: linear-gradient(
      140deg
      ,#be10e1 0%,#BC70A4 50%,#d641bd 75%) !important;
  }
  .content h2, .content p {
    color: #fff;
    text-align: center;
  }
  .content h2 {
    font-size: 24px;
  }
  .content p {
    font-size: 18px;
  }
  .awardstyle {
    width: 200px;
    margin: 0 auto;
  }
  .buttonstyle {
    position: absolute;
    top: -75px;
    right: 0;
  }
  .buttonstyle button {
    margin: 0 20px;
  }
  .sidebar {
    background-color: #00000026;
    height: 100%;
    z-index: 2;
  }
  .sidebar img {
    width: 200px;
    padding-top: 80px;
  }
  .sidebar h1 {
    font-family: 'Allison',cursive;
    font-size: 35px;
    position: absolute;
    top: 29%;
    left: 46px;
  }
  .btn-style button, .btn-style img, .btn-style h1  {
    display: block;
    margin: 0 auto;
    color: #fff;
  }
  .btn-style a {
    color: #fff;
  }
  .carousel-item.active {
    display: flex !important;
}
.carousel-control-prev {
  left: 10%;
}
a:focus, input:focus, button:focus, select:focus, textarea:focus {
  outline: none !important;
  box-shadow: none !important;
}
a {
  text-decoration: none;
}
  .home-content-logo{
    img{
      width: 150px;
    @media only screen and (min-width:576px){
      width: auto;
    }
    } 
  }
  .home-content {
    width: 100%;
  }
  
  .home-signin-title {
    padding-top: 22px;
    border-bottom: 1px solid #c10e21;
    padding-bottom: 17px;
    margin-bottom: 20px;
    @media only screen and (min-width:576px){
      padding-top: 19px;
      margin-bottom: 28px;
    }
    @media only screen and (min-width:768px){
      padding-top: 28px;
    }
    @media only screen and (min-width:992px){
      padding-top: 58px;
    }

  }
  .home-helptext {
    font-size: 12px;
    @media only screen and (min-width:576px){
      font-size: 14px;
    }
  }
  .home-submit {
    padding-top: 8px;
    > *{
      display: block;
      width: 100%;
      text-align: center;
      @media only screen and (min-width:576px){
        display: inline-block;
        width:auto;
      }
    }
      @media only screen and (min-width:576px){
        padding-top: 18px;
      }
      @media only screen and (min-width:768px){
        padding-top: 22px;
      }
      @media only screen and (min-width:992px){
        padding-top: 12px;
      }
  }
  .home-fp {
    color: #c10e21;
  }
  }
`;
