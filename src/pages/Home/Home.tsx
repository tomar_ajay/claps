/* eslint-disable no-restricted-globals */
/* eslint-disable no-debugger, no-console */
import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { SignInWrapper } from "./styled";

const HomePage = () => {
  return (
    <SignInWrapper>
      <div className="maindiv">
        <div className="video-container">
          <div className="overlay" />
          <video loop autoPlay controls muted width="100%">
            <source src="../images/loginvideo1.mp4" type="video/mp4" />
            <track
              src="../images/loginvideo.mp4"
              kind="captions"
              srcLang="en"
              label="english_captions"
            />
          </video>
        </div>
        <div className="home-wrapper position-absolute">
          <div className="home-wrapper-left" role="presentation">
            <img src="../../images/logo.png" alt="Claps Logo" />
            <h1>Claps</h1>
            <p>Recognition and Rewards for All!</p>
          </div>
          <div className="home-wrapper-right">
            <div className="position-relative">
              <div className="buttonstyle">
                <Button variant="contained" color="default">
                  <Link to="/rewards">Rewards</Link>
                </Button>
                <Button variant="contained" color="default">
                  <Link to="/dashboard">Login</Link>
                </Button>
              </div>
            </div>
            <div className="home-content">
              <div>
                <div className="justify-content-between align-items-center">
                  <h1 className="signin mb-0 font-400">
                    Impelsys Rewards Program
                  </h1>
                  <p className="mt-4">
                    Impelsys Rewards Program is a portal that gives every
                    employer and employee a modem where they can be recognized,
                    by their seniors for all the efforts they have done in
                    contributing to the success of Impelsys.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </SignInWrapper>
  );
};

export default HomePage;
