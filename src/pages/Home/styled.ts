import styled from "styled-components";

export const SignInWrapper = styled.div`
  .maindiv {
    height: 100vh;
    overflow:hidden;
  }
  .video-container {
    position: relative;
    width: 100%;
    height: auto%;
    background-attachment: scroll;
    overflow: hidden;
  }
  .video-container video {
    min-width: 100%;
    position: relative;
  }
  .video-container .overlay {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 2;
    background: #00000061;
    opacity: 0.5;
  }
  .home-wrapper {
    width: 100%;
    min-height: 100vh;
    position: relative;
    top: 0;
    display: flex;
    @media only screen and (min-width:992px){
      display: flex;
      top: 0;
    }
  }
  .home-wrapper-left {
    min-height: 345px;
    background-size: cover;
    padding: 15px;
    position: relative;
    z-index: 2;
    @media only screen and (min-width:576px){
      padding: 20px;
      height: 472px;
    }
    @media only screen and (min-width:768px){
      padding: 30px;
      height: 551px;
    }
    @media only screen and (min-width:992px){
      width: 40%;
      height: auto;
      display: block;
      justify-content: center;
      align-items: center;
      margin-bottom: 0px;
      text-align: center;
      padding-top: 100px;
    }
    img {
      height: 112px;
      display: block;
      margin: 0 auto;
      @media only screen and (min-width:576px){
        height: 122px;
      }
      @media only screen and (min-width:768px){
        height: 160px;
      }
      @media only screen and (min-width:992px){
        height: 260px;
      }
    }
  }
  .home-wrapper-left h1 {
    font-family: 'Allison', cursive;
    font-size: 28px;
    position: absolute;
    top: 12%;
    left: 29%;
    color: #fff;
    @media only screen and (min-width:768px){
      font-size: 45px;
      position: absolute;
      top: 24%;
      left: 27%;
    }
    @media only screen and (min-width:1200px){
      font-size:80px;
      top: 42%;
      left: 29%;
    }
  }
  .home-wrapper-left p {
    font-size: 15px;
    color: #fff;
    @media only screen and (min-width:768px){
      font-size: 24px;
      padding-top: 25px;
    }
    @media only screen and (min-width:1200px){
      font-size: 30px;
    }
  }
  .home-wrapper-right {
    height: auto;
    border-radius: 11px;
    opacity: 1;
    display: block;
    padding: 36px 24px;
    justify-content: center;
    align-items: center;
    z-index: 2;
    
    @media only screen and (min-width:576px){
      padding: 44px 36px;
     
    }
    @media only screen and (min-width:768px){
      padding: 80px 36px;
     
    }
    @media only screen and (min-width:992px){
      min-height: 100vh;
      width: 50%;
      position: static;
      padding: 70px;
      border-radius: 0;
      box-shadow: none;
    }
    @media only screen and (min-width:1200px){
      min-height: 100vh;
      width: 60%;
      position: static;
      padding: 110px;
      border-radius: 0;
      box-shadow: none;
    }
  }
  .home-content {
    background: rgba(0,0,0,0.1);
    padding: 20px;
    border-radius: 5px;
    color:white;
  }
  .MuiButton-contained {
    background: linear-gradient(
      140deg
      ,#be10e1 0%,#BC70A4 50%,#d641bd 75%) !important;
  }
  .buttonstyle {
    position: absolute;
    top: -75px;
    right: 0;
    @media only screen and (min-width:768px){
      top: -48px;
    }
    @media only screen and (min-width:1200px){
      top: -75px;
    }
  }
  .buttonstyle a {
    color: #fff;
    text-decoration: none;
  }
  .buttonstyle button {
    margin: 0 20px;
  }
  .home-content-logo{
    img{
      width: 150px;
    @media only screen and (min-width:576px){
      width: auto;
    }
    } 
  }
  .home-content {
    width: 100%;
  }
  .signin{
    font-size: 22px;
    @media only screen and (min-width:576px){
      font-size: 26px;
    }
    @media only screen and (min-width:768px){
      font-size: 34px;
    }
  }
  .home-signin-title {
    padding-top: 22px;
    border-bottom: 1px solid #c10e21;
    padding-bottom: 17px;
    margin-bottom: 20px;
    @media only screen and (min-width:576px){
      padding-top: 19px;
      margin-bottom: 28px;
    }
    @media only screen and (min-width:768px){
      padding-top: 28px;
    }
    @media only screen and (min-width:992px){
      padding-top: 58px;
    }

  }
  .home-helptext {
    font-size: 12px;
    @media only screen and (min-width:576px){
      font-size: 14px;
    }
  }
  .home-submit {
    padding-top: 8px;
    > *{
      display: block;
      width: 100%;
      text-align: center;
      @media only screen and (min-width:576px){
        display: inline-block;
        width:auto;
      }
    }
      @media only screen and (min-width:576px){
        padding-top: 18px;
      }
      @media only screen and (min-width:768px){
        padding-top: 22px;
      }
      @media only screen and (min-width:992px){
        padding-top: 12px;
      }
  }
  .home-fp {
    color: #c10e21;
  }
  }
`;
export const AnotherWraper = styled.div``;
