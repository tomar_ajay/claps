const config: any = {
  env: process.env.REACT_APP_MY_ENV || "dev",
  dev: {
    agreementFilePath: "https://d17zibtpdnrjam.cloudfront.net",
    apiEnspoints: {
      accounts:
        "https://xnzrjvorab.execute-api.us-east-1.amazonaws.com/dev/accounts",
      catalogs:
        "https://b57v0tp0db.execute-api.us-east-1.amazonaws.com/dev/catalogs",
      documents:
        "https://i3gg3pt4x5.execute-api.us-east-1.amazonaws.com/dev/documents",
    },
  },
  qa: {
    agreementFilePath: "https://d17zibtpdnrjam.cloudfront.net",
    apiEnspoints: {
      accounts:
        "https://p9vfkzggoj.execute-api.us-east-1.amazonaws.com/qa/accounts",
      catalogs:
        "https://ao06yzf0o0.execute-api.us-east-1.amazonaws.com/qa/catalogs",
      documents:
        "https://967l5go2bi.execute-api.us-east-1.amazonaws.com/qa/documents",
    },
  },
  stg: {
    agreementFilePath: "https://d17zibtpdnrjam.cloudfront.net",
    apiEnspoints: {
      accounts:
        "https://v637zko3jg.execute-api.us-east-1.amazonaws.com/stg/accounts",
      catalogs:
        "https://yfgw4r7a6b.execute-api.us-east-1.amazonaws.com/stg/catalogs",
      documents:
        "https://5qv4jbusic.execute-api.us-east-1.amazonaws.com/stg/documents",
    },
  },
  prod: {
    agreementFilePath: "https://d17zibtpdnrjam.cloudfront.net",
    apiEnspoints: {
      accounts:
        "https://rvpsnl74fg.execute-api.us-east-1.amazonaws.com/prod/accounts",
      catalogs:
        "https://e9mp9w0qd6.execute-api.us-east-1.amazonaws.com/prod/catalogs",
      documents:
        "https://6937il0r7l.execute-api.us-east-1.amazonaws.com/prod/documents",
    },
  },
  local: {
    agreementFilePath: "https://d17zibtpdnrjam.cloudfront.net",
    apiEnspoints: {
      accounts: "http://localhost:3001/accounts",
      catalogs: "http://localhost:3002/catalogs",
      documents: "http://localhost:3003/documents",
    },
  },
};

export default config;
